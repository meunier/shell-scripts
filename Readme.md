# Shell scripts for development environment and other stuff

* **[Media pause on windows lock](media-pause-on-lock/Readme.md)**  
Will automatically pause any media playing (like Spotify) when locking the computer.

* **[Stream Line Highlighter](streamline-highlighter)**  
Helper script that will highlight and filter some lines from a stream.

* **[Kill Explorer Windows](kill-explorer-windows)**  
Run this helper script to close all Explorer windows.

* **[View process memory](view-process-memory)**  
Show a summary of process (grouped by name) about their memory usage.