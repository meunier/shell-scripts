Get-Process `
	| Group-Object -property ProcessName `
	|% {
		$x=[ordered]@{Name=$_.Name; Count=$_.Count};
		$_.Group | Measure-Object -Property WS,PM,NPM,VM -Sum |% { $x[$_.Property] = $_.Sum/1MB };
		[PsCustomObject]$x
	} `
	| Sort-Object -Property PM -Desc `
	| Out-GridView -Title "Process memory" -Wait