# View process memory

Helper script that will show a summary of process (grouped by name) about their memory usage.
The native task manager of Windows will only show the memory size by process, but not by group of same process (like firefox for example).

**Screenshot:**  
![screenshot](./Screenshot.png)

## Prerequisistes:

* Windows 10

## Usage

Double-click on script `view-process-memory.bat`
