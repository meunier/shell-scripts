using System;
using System.Runtime.InteropServices;

public class MediaPause {
	public enum INPUT_TYPE : uint {
		INPUT_MOUSE = 0,
		INPUT_KEYBOARD = 1,
		INPUT_HARDWARE = 2
	}
	
	[StructLayout(LayoutKind.Explicit, Pack = 1)]
	public struct KEYBDINPUT {
		[FieldOffset(0)]
		public ushort wVk;
		[FieldOffset(2)]
		public ushort wScan;
		 [FieldOffset(4)]
		public uint dwFlags;
		[FieldOffset(12)]
		public uint time;
		[FieldOffset(20)]
		public uint Padding1;
		[FieldOffset(28)]
		public uint Padding2;
	}
	
	[StructLayout(LayoutKind.Explicit, Pack = 1)]
	public struct INPUT {
		[FieldOffset(0)]
		public INPUT_TYPE type;
		[FieldOffset(8)]
		public KEYBDINPUT kb;
	}

	static public ushort VK_MEDIA_STOP = 0xB2;
	static public uint KEYEVENTF_EXTENDEDKEY = 0x0001;
	static public uint KEYEVENTF_KEYUP = 0x0002;

	[DllImport("user32.dll", SetLastError = true)]
	public static extern UInt32 SendInput(UInt32 nInputs, [MarshalAs(UnmanagedType.LPArray, SizeConst = 1)] INPUT[] pInputs, Int32 cbSize);

	[DllImport("kernel32.dll", CharSet=CharSet.Auto)]
	public static extern uint GetLastError();
	
	public static long process() {
		var pInputs = new[] {
			new INPUT() {
				type = INPUT_TYPE.INPUT_KEYBOARD,
				kb = new KEYBDINPUT() {
					wScan = (ushort)0x0024,
					wVk = VK_MEDIA_STOP,
					dwFlags = KEYEVENTF_EXTENDEDKEY
				}
			},
			
			new INPUT() {
				type = INPUT_TYPE.INPUT_KEYBOARD,
				kb = new KEYBDINPUT() {
					wScan = (ushort)0x0024,
					wVk = VK_MEDIA_STOP,
					dwFlags = KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP
				}
			}
		};
		
		uint nEvents = SendInput(1, pInputs, Marshal.SizeOf(typeof(INPUT)));
		if (nEvents == 0)
		{
			Console.WriteLine("SendInput = {0}", GetLastError());
			return -2;
		}

		return 0;
	}
}