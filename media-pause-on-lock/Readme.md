# Media pause on Windows lock

Will automatically pause any media playing (like Spotify) when locking the computer.

**Requirements:**
- Windows 10

## Installation

1. Open the task scheduler (or run taskschd.msc)
2. Click on "Create Task"
  * Name: "Media pause" or what you want
  * Security options / Run with highest privileges: true
3. From _Triggers_ tab, click on _New_ button:
  * Begin the task: "On workstation lock"
  * Enabled: true
  * On time: true
4. From _Actions_ tab, click on _New_ button:
  * Action: "Start a program"
  * Program/script: "powershell"
  * Arguments: "-nop -File media-pause.ps1"
  * Start in: <directory/path/to/media-pause.ps1>
5. From _Conditions_ tab:
  * Power / Start the task only if the computer is on AC power: false