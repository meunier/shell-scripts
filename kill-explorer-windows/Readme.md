# Close All Explorer Windows

Run this helper script to close all Explorer windows (excepting processes of the desktop and taskbar).

## Requirements
- Windows 10

## Installation

- Create a shortcut on this script with command line:  
`powershell -nop -WindowStyle Hidden -File "kill-explorer-windows.ps1"`