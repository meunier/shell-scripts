# libraries => ::{031E4825-7B94-4DC3-B131-E946B44C8DD5}
# my-computer => ::{20D04FE0-3AEA-1069-A2D8-08002B30309D}
$windows = (New-Object -comObject Shell.Application).Windows() | where-object {
	$_.LocationURL -ne "" `
	-or $_.Document.Folder.Self.Path -like "::{031E4825-7B94-4DC3-B131-E946B44C8DD5}" `
	-or $_.Document.Folder.Self.Path -like "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
}
$n = ($windows | measure).Count
if ($n -gt 0) {
	Write-Host "kill " $n " windows:"
	$windows | select-object -first $n | foreach-object { 
		Write-Host "* " $_.LocationName
		$_.quit() 
	}
}