#!/bin/perl
use Term::ANSIColor;
my $fl=$ARGV[0];
my $hl=$ARGV[1];
my $buf = "";
print "filtering: $fl\n";
print "highlighting: $hl\n";
while(sysread(STDIN,$buf,512,length($buf))) {
	#print("---\n");
	while ($buf =~ s/^([^\n]*\n)//) {
		my $line=$1;
		if ($line =~ /$fl/i) {
			if ($line =~ /$hl/i) {
				print colored(["bold red"], $line);
			} else {
				print $line;
			}
		}
	}
}