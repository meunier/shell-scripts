# Stream Line Highlighter

Helper script that will highlight and filter some lines from a stream.  
This script is compatible with `adb logcat` where a simple `grep` command doesn't work.

## Prerequisistes:

* perl

## Usage

_perl `highlight.pl` \<filtering-pattern\> \<highlight-pattern\>_
- `<filtering-pattern>` is acting like grep, it's a regex pattern with insensitive case.
- `<highlight-pattern>` is colouring lines matching this pattern, it's a regex pattern with insensitive case.

## Examples

- ```tail -f my.log | perl highlight.pl 'MY_TAG' '(error|warning)'```
- ```adb logcat | perl highlight.pl 'MY_TAG' '(E|W):'```